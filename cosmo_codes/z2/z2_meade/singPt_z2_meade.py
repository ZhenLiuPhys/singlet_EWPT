from __future__ import division

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 20:33:49 2018

@author: yik
"""

import baseMo_z2_meade as bm

from scipy.optimize import root

ls = 1.5

lm = 8.6

q = 650.


def b(muh2, mus2, lh, ls, lm, q):
    
    m = bm.modelz2(muh2, mus2, lh, ls, lm, q)
    
    v = m.findMinimum([0.,0.],0.)
    
    mh = m.d2V(v, 0.)[0][0]**.5
    
    ms = m.d2V(v, 0.)[1][1]**.5
    
    return v, mh, ms
    

def equations(p):   
    
    muh2, mus2, lh = p
        
    v, mh, ms = b(muh2, mus2, lh, ls, lm, q)
        
    return (v[0] - 246., mh - 125., ms - q)

    
#muh2, mus2, lh =  root(equations, (4000.,	5.*100.**2.,	0.1)).x
 
muh2, mus2, lh =  root(equations, (-5000.,100000.,0.1)).x
    

print muh2

print mus2

print lh

v, mh, ms = b(muh2, mus2, lh, ls, lm, q)

print v

print mh

print ms

print ls

print lm

print q
    
m = bm.modelz2(muh2, mus2, lh, ls, lm, q)

print("\n")
print("\n")
"""
print("The T=0 potential of the model reads")

bm.vsh(m, (-300, 300, -30, 30), 0)
"""
print("\n")
print("\n")

print("Now let's find the phase transitions:")

#m.calcTcTrans()

print("\n \n All the phase transitions of such a model are")

#m.prettyPrintTcTrans()

print("And the T-dependent 'phase norm' reads")

#plt.figure()
#m.plotPhasesPhi()
#plt.show()

#plt.figure()
#m.plotPhases2D()
#plt.show()

"""
Note: to be completed: 
models may have probolems calculating tunneling (possibly due to the strength of phase transitions).
We need Tc info instead of Tn info. 
So such a step shall be neglected at this point.

print("\n \n")

print("Now let's find the corresponding tunneliings:")

m.findAllTransitions()

print("\n \n All the tunnelings/phase transitions of such a model are")

m.prettyPrintTnTrans()

"""

