from __future__ import division

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 20:33:49 2018

@author: yik
"""

import baseMo_s_b_cwd as bm

import matplotlib.pyplot as plt

m = bm.model(8705.9741461, -15628.7060797,0.0599421, 0.5277367, 0.5144458, v2re = 1000.**2.)

print("\n")
print("\n")

print("The T=0 potential of the model reads")

# bm.vsh(m, (-300., 300., -30., 30.), 0.)

print("\n")
print("\n")

print("Now let's find the phase transitions:")

m.calcTcTrans()

print("\n \n All the phase transitions of such a model are")

m.prettyPrintTcTrans()

print("And the T-dependent 'phase norm' reads")

plt.figure()
m.plotPhasesPhi()
plt.show()

plt.figure()
m.plotPhases2D()
plt.show()

"""
Note: to be completed: 
models may have probolems calculating tunneling (possibly due to the strength of phase transitions).
We need Tc info instead of Tn info. 
So such a step shall be neglected at this point.

print("\n \n")

print("Now let's find the corresponding tunneliings:")

m.findAllTransitions()

print("\n \n All the tunnelings/phase transitions of such a model are")

m.prettyPrintTnTrans()

"""

