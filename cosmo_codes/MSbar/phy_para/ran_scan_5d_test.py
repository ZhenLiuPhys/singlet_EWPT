#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 11 17:10:18 2018

@author: yik
"""

"""
In this module, we create a scaning over the parameter space (ms, sint, tanb) which enters the model 
defined and used in the module baseMo.py

We create a dictionary containing the parameter info and the 1st order phase transition info, including 
critical temperature, high low T vevs, and the phase transition strength. 

Usage functions are created to print/plot the scaning. 
"""


import baseMo_s_cw as bmcw

import baseMo_s_cwd as bmcwd



import random as ran

import numpy as np

import time

import os


import mpi4py.MPI as mpi

import sys

import math

comm = mpi.COMM_WORLD

FILE = 'test'

#msr = [125., 500.]

msr = [0., 600.]

logtanbr = [-2.,1.5]

sintr = [-0.6,0.6]

mhr = [100., 150.]

vr = [200., 340.]

npt = 10000

scan = []


def physpara(m):
    
    vp = m.findMinimum(T=0.)
    
    v2phy = vp[0]**2.
    
    tanbphy = vp[1]/vp[0]
    
    m2phys = m.d2V(vp, T=0.)
    
    m2physd = np.linalg.eigvals(m2phys)
    
    if m.ms >= m.mh:
        
        msphy = max(m2physd)**0.5
        
        mhphy = min(m2physd)**0.5
    
    else:
        
        msphy = min(m2physd)**0.5
        
        mhphy = max(m2physd)**0.5
        
        
    sintphy = math.sin(.5*math.asin(2.*m2phys[0][1]/(msphy**2. - mhphy**2.)))
    
    
    return [msphy, tanbphy, sintphy, mhphy, v2phy]
        
        

def trans(i, m):

    print "\n check point %s with \n" %[i, m.ms, m.tanb, m.sint, m.mh, m.v2] 
    
    m.calcTcTrans() 

    trans = m.TcTrans
    
    check = False
    
    sfoph = []
    
    for k in range(len(trans)):
        tc = trans[k]['Tcrit']
        sh = abs(trans[k]['low_vev'][0]-trans[k]['high_vev'][0])/tc
        if trans[k]['trantype'] == 1 and sh >= 1.: 
            sfoph.append([tc, sh])      
               
    for key in m.phases:
        if m.phases[key].check:               
            check = True
    
    return trans, sfoph, check
    
def t0vev(m):
    
    wvev = m.Vtot(m.findMinimum(T=0.),T=0.) - m.Vtot(m.findMinimum(X=[0.,0.],T=0.),T=0.) > 1.
    
    return wvev


def thvev(m):
    
    htX =  m.findMinimum(T=1000.)
    
    wvev = (abs(htX[...,0]) > 10.**10.) or (abs(htX[...,1]) > 10.**10.)
    
    return wvev


def getscani(i, m):
    
    
    if any([m.l1 > 4.*np.pi/3., m.l2 > 4.*np.pi/3., m.lm > 16.*np.pi/3.]): 
           
        print('wrong paras')
        
        scani = None
        
        
    else: 
            
        scani = []
            
        scani.append([m.ms, m.tanb, m.sint, m.mh, m.v2])
             
        scani.append([m.l1, m.l2, m.lm, m.m12, m.m22])
        
        scani.append(physpara(m))
            
        if t0vev(m) or thvev(m):
            
            scani.append([])
            
            scani.append([])
            
            scani.append(True)
                
        else:

            transit, sfoph, check = trans(i, m)
            
            scani.append([sfoph])
            
            scani.append([transit])
            
            scani.append(check)
         
    return scani
                
                                                  

def getscan(msbox, logtanbbox, sintbox, mhbox, vbox, npt):
 
    msmin,msmax = msbox
    logtmin, logtmax = logtanbbox
    smin, smax = sintbox
    mhmin, mhmax = mhbox
    vmin, vmax = vbox
        
    scan_task = range(npt)
    
    rank = comm.Get_rank()

    size = comm.Get_size()
        
    scan_rank = []
                
    ran.seed(time.time() + rank)
        
    for n in scan_task:
        
        if n%size != rank: 
           continue
       
        else:
                
            ms0 = ran.uniform(msmin,msmax)
            logtanb0 = ran.uniform(logtmin, logtmax)
            tanb0 = 10.**logtanb0
            sint0 = ran.uniform(smin, smax)
            mh0 = ran.uniform(mhmin, mhmax)
            v0 = ran.uniform(vmin, vmax)
            
            v2re = 1000.**2.
            
            mcw = bmcw.model(ms0, tanb0, sint0, mh0, v0**2., v2re)
            
            mcwd = bmcwd.model(ms0, tanb0, sint0, mh0, v0**2., v2re)
            
            mhphys = physpara(mcwd)[-2]
            
            vphys = physpara(mcwd)[-1]**.5
                        
            if all((vphys <= 248., vphys >= 244., mhphys <= 127., mhphys >= 123.)):
                
                print "Scan number %d being done by processor %d of %d" % (n, rank, size)
                
                scancw = getscani(n, mcw)
                                
                scancwd = getscani(n, mcwd)
                
                scan_rank.append([scancw, scancwd])
              
                filename = '%s_%s' % (FILE, rank)
              
                np.save(filename, scan_rank)
                       
            else:
                
                print '.'
                
                pass
            
                    
                                 
scan = getscan(msr, logtanbr, sintr, mhr, vr, npt)

# np.load("ran_scan_1.npz")

#npfile.files

#npfile['check']

# npfile['scan_1pht'].item().get('sint')






