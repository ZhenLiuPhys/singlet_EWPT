#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 14 17:52:23 2018

@author: yik
"""

import baseMo_s_cwd as bm

import baseMo_s_t as bmt

import numpy as np

from scipy.optimize import root

import matplotlib.pyplot as plt

from pylab import meshgrid,cm,imshow,contour,clabel,colorbar,axis,title,show


class findmhv:
    
    def __init__(self, ms, tanb, sint, v2re):
        
        self.ms = ms
        
        self.tanb = tanb
        
        self.sint = sint
        
        self.v2re = v2re
        
        
    def mhv(self, v0, mh0):
    
        m = bm.model(self.ms, self.tanb, self.sint, mh0, v0**2., self.v2re)
    
        vp = m.findMinimum(T=0.)
    
        ms2d = np.linalg.eigvals(m.d2V(vp,T=0.))
        
        msmax = max(ms2d)**0.5
        msmin = min(ms2d)**0.5
    
        if self.ms < mh0:
            return vp, msmax, msmin
        if self.ms >= mh0:
            return vp, msmin, msmax

    def mhv0(self, v0, mh0):
    
        m = bmt.model(self.ms, self.tanb, self.sint, mh0, v0**2., self.v2re)
    
        vp = m.findMinimum(T=0.)
    
        ms2d = np.linalg.eigvals(m.d2V(vp,T=0.))
        
        msmax = max(ms2d)**0.5
        msmin = min(ms2d)**0.5
    
        if self.ms < mh0:
            return vp, msmax
        if self.ms >= mh0:
            return vp, msmin


    def equations(self, p):   
    
        v0, mh0 = p
        
        v, mh, ms = self.mhv(v0, mh0)
        
        return (v[0] - 246., mh - 125.)
    
    
    def looping(self, vgs, mhgs, r, d, dis):
    
        for i in range(int(r/d)):
        
            v0 = vgs - r/2. + i*d
        
            for j in range(int(r/d)):
            
                mh0 = mhgs - r/2. + j*d
            
                vp, mhp, msp = self.mhv(v0, mh0)
            
                if mhp >= 125. - dis/2. and mhp <= 125. + dis/2. and vp[0] >= 246. - dis/2. and vp[0] <= 246. + dis/2.:
                
                    vi = v0
                    mhi = mh0
                
                   # print 'find'
                   # print mh0
                   # print mhp
                   # print v0
                   # print vp[0]
                                      
                
                    break
                else:
                    
                    vi, mhi = None, None
                
            else:
                continue
            break
        
        return vi, mhi
    

    
    def solving(self, vgs, mhgs):
        
        out =  root(self.equations, (vgs, mhgs)).x
        
        print self.equations(out)
        
        if abs(self.equations(out)[0]) <= 1. and abs(self.equations(out)[1]) <= 1.:
            
            return out, True
        
        else:
            
            return out, False
        
    
    
    def find(self):
        
  
        out, suc = self.solving(246., 125.)
        
        if suc == True:
            
            return out
            
        else:
    
            vgs, mhgs = out
        
            vi, mhi = self.looping(vgs, mhgs, 50, 2, 5)
        
            
            if vi != None and mhi != None:
                
                out, suc =  self.solving(vi, mhi)
                
                if suc == True:
                    
                    return out
                    
                
                else:
                    
                    vgs, mhgs = out
                    
                    vi, mhi = self.looping(vgs, mhgs, 10, 0.5, 2)
                    
                    
                    if vi != None and mhi != None:
                        
                        out, suc =  self.solving(vi, mhi)
                                          
                        if suc == True:
            
                            return out
                              
                        else:     
                            
                            return [None,None]
                
                    else:
                        
                        return [None,None]
            
            else:
                
                return [None,None]
                
                
def plvmh(f, box, n=50, clevs=200, cfrac=.8, **contourParams):
    pt = []
    xmin,xmax,ymin,ymax = box
    x = np.arange(xmin, xmax, 1.)
    y = np.arange(ymin, ymax, 1.)
    for v0 in x:
        for mh0 in y:
            vp, mhp, msp = f.mhv(v0, mh0)
            pt.append([v0, mh0, vp[0],mhp])
    np.savetxt('func', pt)               
                
                 
        
        
        
        
    
"""
for i in range(10):
    
    for j in range(10):
        
        if vi == None or mhi == None:
            
            vi, mhi = looping(10 + i*10, 10 + j*10, 1, 1)
            
            print 'loop done'
            print vi
            
        else:
            
            print 'find'
            
            print vi
            
            break
    else:
        continue
    break
 """       
        






