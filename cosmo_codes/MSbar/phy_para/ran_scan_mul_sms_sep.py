#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 11 17:10:18 2018

@author: yik
"""

"""
In this module, we create a scaning over the parameter space (ms, sint, tanb) which enters the model 
defined and used in the module baseMo.py

We create a dictionary containing the parameter info and the 1st order phase transition info, including 
critical temperature, high low T vevs, and the phase transition strength. 

Usage functions are created to print/plot the scaning. 
"""

import baseMo_s_t as bmt

import baseMo_s_cw as bmcw

import baseMo_s_d as bmd

import baseMo_s_cwd as bmcwd

import find



import random as ran

import numpy as np

import time

import os


import mpi4py.MPI as mpi

import sys


comm = mpi.COMM_WORLD

FILE = sys.argv[1]

BASE_PATH = '/home/yikwang/ewpht_data/'

DATA_PATH = '/data/yikwang/'

#msr = [125., 500.]

msr = [0., 125.]

logtanbr = [-2.,1.5]

sintr = [-0.4,0.4]

npt = int(sys.argv[2])

scan = []

def trans(i, m, ms, tanb, sint, mh = 125., v2 = 246.**2.):

    print "\n check point %s with \n" %[i, ms, tanb, sint] 
    m.calcTcTrans() 

    trans = m.TcTrans
    
    check = False
    
    sfoph = []
    
    for k in range(len(trans)):
        tc = trans[k]['Tcrit']
        sh = abs(trans[k]['low_vev'][0]-trans[k]['high_vev'][0])/tc
        if trans[k]['trantype'] == 1 and sh >= 1.: 
            sfoph.append([tc, sh])      
               
    for key in m.phases:
        if m.phases[key].check:               
            check = True
    
    return trans, sfoph, check
    
def t0vev(m, tanbp):
    
    wvev = m.Vtot([246.,246.*tanbp],T=0.) - m.Vtot(m.findMinimum(X=[0.,0.],T=0.),T=0.) > 1.
    
    return wvev


def thvev(m):
    
    htX =  m.findMinimum(T=1000.)
    
    wvev = (abs(htX[...,0]) > 10.**10.) or (abs(htX[...,1]) > 10.**10.)
    
    return wvev


def getscani(i, bm, ms, tanb, sint, mh, v2, msp, tanbp, v2re):
    
                
    m = bm.model(ms, tanb, sint, mh, v2, v2re)
        
    if any([m.l1 > 4.*np.pi/3., m.l2 > 4.*np.pi/3., m.lm > 16.*np.pi/3.]): 
           
        print('wrong paras')
        
        scani = None
        
        
    else: 
            
        scani = []
            
        scani.append([ms, tanb, sint, mh, v2, msp, tanbp])
             
        scani.append([m.l1, m.l2, m.lm, m.m12, m.m22])
            
        if t0vev(m,tanbp) or thvev(m):
            scani.append([])
            
            scani.append([])
            
            scani.append(True)
                
        else:

            transit, sfoph, check = trans(i, m, ms, tanb, sint)
            
            scani.append([sfoph])
            
            scani.append([transit])
            
            scani.append(check)
         
    return scani
                
                                                  

def getscan(msbox, logtanbbox, sintbox, npt):
 
    msmin,msmax = msbox
    logtmin, logtmax = logtanbbox
    smin, smax = sintbox
        
    scan_task = range(npt)
    
    rank = comm.Get_rank()

    size = comm.Get_size()
        
    scan_rank = []
                
    ran.seed(time.time() + rank)
        
    for n in scan_task:
        
        if n%size != rank: 
           continue
       
        else:
            print "Scan number %d being done by processor %d of %d" % (n, rank, size)
                
            ms0 = ran.uniform(msmin,msmax)
            logtanb0 = ran.uniform(logtmin, logtmax)
            tanb0 = 10.**logtanb0
            sint0 = ran.uniform(smin, smax)
            
            if ms0 >= 246.:
                v2re = ms0**2.
            else:
                v2re = 246.**2.
                
            try:
           
                f = find.findmhv(ms0, tanb0, sint0, v2re)
                bare = f.find()
            
                if bare[0] == None:
                    pass
            
                else:   
                
                    mh0 = bare[1]                
                    v20 = bare[0]**2.
            
                    vp, mhp, msp = f.mhv(bare[0],bare[1])              
                    v2p = vp[0]**2.               
                    tanbp = vp[1]/vp[0]
             
                    scant = getscani(n, bmt, ms0, tanb0, sint0, mhp, v2p, ms0, tanb0, v2re)
                
                    scancw = getscani(n, bmcw, ms0, tanb0, sint0, mh0, v20, msp, tanbp, v2re)
                
                    scand = getscani(n, bmd, ms0, tanb0, sint0, mhp, v2p, ms0, tanb0, v2re)
                
                    scancwd = getscani(n, bmcwd, ms0, tanb0, sint0, mh0, v20, msp, tanbp, v2re)
                
                    scani = [scant, scancw, scand, scancwd]
        
                    scan_rank.append(scani)
              
                    filename = '%s_%s' % (FILE, rank)
              
                    np.save(os.path.join(BASE_PATH, filename), scan_rank)
            
                    np.save(os.path.join(DATA_PATH, filename), scan_rank) 
                    
            except Exception:
                
                pass

                                 
scan = getscan(msr, logtanbr, sintr, npt)

# np.load("ran_scan_1.npz")

#npfile.files

#npfile['check']

# npfile['scan_1pht'].item().get('sint')






