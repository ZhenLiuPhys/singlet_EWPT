#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 11 17:10:18 2018

@author: yik
"""

"""
In this module, we create a scaning over the parameter space (ms, sint, tanb) which enters the model 
defined and used in the module baseMo.py

We create a dictionary containing the parameter info and the 1st order phase transition info, including 
critical temperature, high low T vevs, and the phase transition strength. 

Usage functions are created to print/plot the scaning. 
"""

import find

import random as ran

import numpy as np

msr = [0.1, 600.]

tanbr = [0.1,10.]

sintr = [-0.3,0.3]

npt = 10000

ranscanslved = []

ranscanno = []

msmin,msmax = msr

tmin, tmax = tanbr

smin, smax = sintr

for i in range(0, npt):
    
    ms0 = ran.uniform(msmin,msmax)
    tanb0 = ran.uniform(tmin, tmax)
    sint0 = ran.uniform(smin, smax)
    if ms0 >= 246.:
        v2re = ms0**2.
    else:
        v2re = 246.**2.
            
    print 'ms=%s, tanb=%s, sint=%s' % (ms0, tanb0, sint0)
    
    try:
        
        f = find.findmhv(ms0, tanb0, sint0, v2re)
        x, slved = f.solving(246., 125.)
        if slved:
            
            vp, mhp, msp = f.mhv(x[0],x[1])
            
            ranscanslved.append([ms0, tanb0, sint0, x[0],x[1],msp,vp[1]/vp[0]])
 
            np.savetxt("ran_scan_slved", ranscanslved)
          
        else:       
                        
            ranscanno.append([ms0, tanb0, sint0])
             
            np.savetxt("ran_scan_no", ranscanno)
            
    except Exception: 
         
        print 'something wrong'
         
        pass
            
                                    

# np.load("ran_scan_1.npz")

#npfile.files

#npfile['check']s

# npfile['scan_1pht'].item().get('sint')






